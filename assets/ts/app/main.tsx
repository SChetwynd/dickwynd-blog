function createElement(
    type: string,
    options: Record<string, string> | null,
    ...content: (undefined | string)[]
): HTMLElement {
    const element = document.createElement(type);
    if (options?.class) {
        for (const className of options.class.split(" ")) {
            element.classList.add(className);
        }
    }
    if (isA(element) && options?.href) {
        element.href! = options.href;
    }
    console.log(content);
    for (const con of (content || [])) {
        element.append(con);
    }
    return element;
}

function isA(element: HTMLElement): element is HTMLAnchorElement {
    return element instanceof HTMLAnchorElement;
}

const React = { createElement };

interface validParams {
    url: string;
    container: HTMLElement;
};

export async function setMentions(url: unknown, container: unknown): Promise<HTMLElement> {
    const validParams = validateParams(url, container);

    await fetch(validParams.url)
        .then((response) => response.json())
        .then((responseJson) => {
            validParams.container.innerText = responseJson.count
        });

    return validParams.container;
}

export async function setComments(url: unknown, container: unknown): Promise<HTMLElement> {
    const validParams = validateParams(url, container);

    await fetch(validParams.url)
        .then((response) => response.json())
        .then((responseJson) => {
            for (let i = 0; i < responseJson.children.length; i ++) {
                const isLastChild = i === responseJson.children.length;
                const response = responseJson.children[i];
                const commentDiv = (
                    <div>
                        <div>
                            <a
                                href={response.author.url}
                                class="underline"
                            >
                                {response.author.name}
                            </a> says:
                        </div>
                        <small>
                            {new Date(response.published).toLocaleDateString()}
                        </small>
                        <div class="space"></div>
                        <small>
                            {response.content.text}
                        </small>
                        { isLastChild
                            ? (
                                <div>
                                    <div class="large-space"></div>
                                        <hr />
                                    <div class="large-space"></div>
                                </div>
                            )
                            : ""
                        }
                    </div>
                );
                validParams.container.append(commentDiv);
            }
        });

    return validParams.container;
}

function validateParams(url: unknown, container: unknown): validParams {
    if (!isString(url)) {
        throw new TypeError("URL must be a string");
    }
    if (!isHTMLElement(container)) {
        throw new TypeError("Expected continer to be a HTML Element");
    }

    return {
        url,
        container
    };
}

function isString(a: unknown): a is string {
    return typeof a === "string";
}

function isHTMLElement(container: unknown): container is HTMLElement {
    return container instanceof HTMLElement;
}
