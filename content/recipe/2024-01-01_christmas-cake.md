---
draft: false
author: Steven
title: Christmas Cake
images: 
  - /images/1704118231242.jpg
ingredients:
  - 1.3kg Dried Fruit*
  - 150ml Spirit**
  - 2 Oranges
  - 250g  butter-like-stuff
  - 250g dark suger
  - 4 large eggs
  - 1 tbsp black treacle
  - 75g chopped blanched
  - almonds
  - 275g plain flour
  - 1 1/2 tsp mixed spice
  - Icing
  - Marzipan
---
\*Any dried fruit will work, the stuff which has been at the back of the cupboard unused for months/years is perfect as long as its not moldy. Try for a high ration of raisins, sultarners, currents, glace cherries are one thing I would always try to include.





\*\*Any flavoured spirit would work, I prefer Brandy. I've tried sherry but it's not great and I thought the cake ended up being rather bland.





1. Put all the dried fruit, into a large mixing bowl, pour over the spirit and stir in the orange zest. Cover with clingfilm and leave to soak over night, or for 3 days if you have the time and patience, stirring daily.
1. Grease and line a 23cm/9in deep, round tin with a double layer of greased greaseproof paper. Preheat the oven to 140C/120C Fan.
1. Measure the butter, sugar, eggs, treacle and almonds into a very large bowl and beat well (preferably with an electric free-standing mixer). Add the flour and ground spice and mix thoroughly until blended. Stir in the soaked fruit. Spoon into the prepared cake tin and level the surface.
1. Bake in the centre of the preheated oven for about 4-4½ hours, or until the cake feels firm to the touch and is a rich golden brown. Check after two hours, if it doesn't look cooked, then its not cooked. A skewer inserted into the centre of the cake should come out clean. Leave the cake to cool in the tin.
1. When cool, pierce the cake at intervals with a fine skewer and feed with a little extra spirit. Feed the cake some extra spirit every 1 or 2 week, being sure to taste the spirit before each feeding to ensure it is still good ;).
1. To marzipan the cake, roll the marzipan, on a surface dusted with icing suger, into a flat disk big enough to cover the top, plus some for the sides, and lay it on top. Some people recomend using melted apricot jam, but really just laying the mazipan on top and giving it a bit of a squeeze seems to secure it on top.
1. If there is excess marzipan around the edges, cut it off, and use it to fill any gaps, and squeeze the cake again to ensure they stay in place.
1. Repeat steps 6 and 7 but with the icing.
1. Enjoy.




