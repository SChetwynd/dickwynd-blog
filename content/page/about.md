{
    "title": "North Cape 4000"
}

This year we are doing <a href="https://www.northcape4000.com/">Noth Cape 4000</a>, a 4000km (2500 miles) cycle event from [Turin, Italy](https://www.openstreetmap.org/relation/43992), to [North Cape, Norway](https://www.openstreetmap.org/way/230772769#map=13/71.1608/25.7916). Its an Ultra Endurance event going through 10 countries, along a set course.

Whilst the challenge is open ended, only those who finish with in the first 25 days get spots on the finishers list, and some will finish in less than 10 days. We must finish or fail in 22 day, why? We must pick Morticia the cat up from the cattery, and I (Steven) must get back to work.

We have very little experience of Ultra Endurance cycling, our longest ride to date is a little over 160km (100 miles), but to reach our goal we will need to do 200km a day. This will leave us a little wiggle room, to deal with any problems we encounter: mechanicals, getting lost, enjoying ourselves too much that kind of thing.

We will both have cameras and mobile phones, and will try to post updates and photos here along the way.

We are raising money for [Alzheimer's Society](https://www.alzheimers.org.uk/) on just giving. My employer [theidol](https://www.theidol.com/) are kindly offering to match the money raised up to £5,000. You can find our page and donate here: [https://www.justgiving.com/fundraising/steven-chetwynd](https://www.justgiving.com/fundraising/steven-chetwynd).
