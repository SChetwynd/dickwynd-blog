{
    "draft": false,
    "disableComments": true,
    "date": "2023-02-22T22:00:00+0000",
    "author": "Steven"
}

<link rel="authorization_endpoint" href="https://auth.skribilo.blog/auth">
<link rel="token_endpoint" href="https://tokens.indieauth.com/token">
<link rel="me" href="mailto:steven@dickwynd.co.uk" />

<span class="p-name u-url u-uid" rel="me" href="{{< permalink >}}">
    <span class="p-given-name">Steven</span>

</span>
