{
    "author": "Clara",
    "date": "Sat, 15 Jul 2023 10:33:13 GMT",
    "tags": [],
    "title": "The Final Campdown",
    "images": [
        "/images/1689414490572-IMG_6527.jpg",
        "/images/1689415674934-IMG_6520.jpg",
        "/images/1689416240692-IMG_6533.jpg"
    ],
    "categories": ["North Cape"]
}

[Its the final Campdown before Europe!](https://www.youtube.com/watch?v=9jK-NcRmVcw)

![](/images/1689414490572-IMG_6527.jpg)

We have less than one week to go before we get on a plane and jet off to Turin and begin the big bike ride of stupidity, also known as NC4000. In my head, the arguments between camping with tent, camping with hammock or bivvy and just not camping and using hotels every night are still very present. So, Steve decided that a overnight camping trip to 'highlight the benefits of a hammock' would be a great idea to see if I would enjoy it, and also if I did enjoy it, to make sure I had a rudimentary understanding of how to set up a hammock. 

That meant another trip back to Hagg's Bank, in Alston. Last time was the beginning of April, and my sleeping bag, rated for around 9C, just did not cut it. This time its July that theoretically my sleeping bag is more suited to this warmer month. So on Thursday (7 days to go) Steve booked a visit to the campsite and on Friday after work, off we went.

The weather was miserable. 

The rain was a persistent horizontal drizzle, and the temperature had been brought down by the wind, and seemed similar to that of April because of it. Haggs Bank seemed to be deserted; a few in the bunkhouse and there were just two tents (one at the top of the site, and one in the woods). The tents seemed to be using every possible guy-line that they had, and the lines were being strained by the wind. I was super impressed by the air-tent which seemed as solid as a rock, I honestly thought it would be flopping about. 

We checked in, and initially pitched the hammocks in the same place as we had used in April (the only place left in the woods since it was so busy). However the wind was just too strong, and we relocated towards the road, where it was slightly more sheltered. This time I had a hammock of my own, and we were trying out the ENO fuse hammock rods; two poles which push the hammocks away from each other so you don't keep bashing into each other. 

The wind was very strong and we also had to use a spare peg we had and a extra random piece of rope we found in a nearby tree to keep the tarp from wildly flapping. It seemed to work well. We also had come up with a cunning plan, of visiting the pub instead of trying to cook meals on a camp stove, and after a quick visit to reserve a table, we went and ate at Nent Hall Hotel down the road. The bar was warm and cosy and by 9.15pm we were getting reluctant to leave again, especially having to return into such wet conditions. We returned and found that only one peg had come loose on the tarp, due to the poor knotwork on the random rope we had discovered: a 1min fix and the whole hammock set up was good to go!

![](/images/1689415674934-IMG_6520.jpg)

Getting in the first time was a little worrying, but it got easier with a few more practices. And as much as I hate to admit it, the hammock was super cosy and warm. The tarp was doing a great job at keeping the rain off us, and the separator bar meant I wasn't bumping into Steve every time I moved, although it did wobble the hammock a little, but that wasn't a huge issue. The only issue was, as they were 'linked', when one person got out, their hammock went up, and the other hammock would automatically go down! A little problematic if you need a 3am toilet visit. 

We settled down for the night, and I was able to drift off relatively fast for a night camping; my watch suggests 11pm-ish. Previous sleeps in the tent have always meant I have a fitful sleep whenever my back comes into contact with a groundsheet, which is always icy cold, and in April that was exactly what happened, alongside just shivering all night in general. I never had this problem in the hammock at all; I did use my air mattress for insulation but the air temperature was warm enough that it was never uncomfortable. I also layered up more, wearing my down jacket inside my sleeping bag, which seemed to be tolerating the warmer conditions better. 

![](/images/1689416240692-IMG_6533.jpg)

If it had been a clear night, there may have been a opportunity to see stars, a possibility which would not been possible in a enclosing tent. It was also possible to stretch out as much as I wished; although not as important for me as it was for Steve. I even found through the night that side sleeping was somewhat possible, although it was easier to sleep on my back mostly. When moving around the whole pitch, it was nice to be able to stand up and have quite a bit of dry space to walk around under. 


There were a few cons; My shoes had to spend the evening on the ground, where they became a campsite for bugs, and they had to be knocked out off, against a tree. The tarp wasn't 100% water resistant, the occasional drop did once or twice find its way to my face. A major con was getting changed meant seeing how flexible I was, while remaining somewhat straight in a hammock. It was doable but not the best. Admittedly it was easier when the sun had set and everything was getting dark, as that meant that other campers were not going to be as entertained. Steve seems to have mastered changing whilst in a sleeping bag and also in the hammock. I will need to try harder. 

After a what turns out to be one of the best nights sleep I have had whilst camping, and waking up at what I thought was 7am, but actually 6, we were able to quickly back up in 30mins and set off home, via the petrol station in Alston. The gentle swaying was quite nice and the fresh breeze meant that it never felt stuffy and smelt like feet which it always seems to when in the tent. 

So, what will I be taking for North Cape 4000? Well... let see how the hammock works out! There is always plan B, hotels all the way, if it fails.

