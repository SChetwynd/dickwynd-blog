---
date: 2023-07-15T16:22:45.713Z
author: Steven
tags: []
draft: false
menu: none
weight: 0
images: []
categories: 
  - North Cape
---

Our location will be shared live with the North Cape 4000 organisers to ensure
there is no foul play, they also open up the tracking for everyone to see,
[here](https://livegps.setetrack.it/Home/IndexMapset?mapset=northcape4000_2023).
Or via the button below.

The tracking site includes the location of all the control points we must visit,
and the route we must take to get there, which we have previously needed to keep
secret.

[![Follow Us](/images/setetrack.png)](https://livegps.setetrack.it/Home/IndexMapset?mapset=northcape4000_2023)
