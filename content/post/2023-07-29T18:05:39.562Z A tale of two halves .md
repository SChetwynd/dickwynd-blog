{
    "author": "Clara",
    "date": "Sat, 29 Jul 2023 18:05:38 GMT",
    "tags": [],
    "title": "A tale of two halves ",
    "images": [
        "/images/1690652784392-IMG_6885.jpeg",
        "/images/1690652863622-IMG_6886.jpeg",
        "/images/1690653021970-IMG_6887.jpeg",
        "/images/1690653157920-IMG_6889.jpeg",
        "/images/1690653275451-IMG_6890.jpeg",
        "/images/1690653370124-IMG_6891.jpeg",
        "/images/1690653545594-IMG_6893.jpeg",
        "/images/1690653748622-IMG_6895.jpeg",
        "/images/1690653907476-IMG_6896.jpeg"
    ],
    "gpx": "/gpx/2023-07-29.gpx",
    "categories": ["North Cape"]
}

A lazy start to the day, after a serious discussion about timings; a friend has calculated we would need to do 255km a day to get to North Cape, and our longest day stands at 165km; it just wouldn’t be achievable. To make ourselves feel a little better we booked our way home via the Amsterdam-Newcastle Ferry which we will get to in 2 weeks. We set out to have an adventure and this is a ferry we both really have wanted to do! Also figuring out how to get to Amsterdam will be exciting. But we still have 2 weeks to see how far we can get!!!


Leaving Paris should have been simple; follow the canal through the suburbs. But the cycle path switches from each side fairly regularly, and we ended up on the footpath side a lot; a very gravelly rough surface, and where Steve managed to get a puncture. Using our tools, we were able to patch it and keep going.


![](/images/1690652784392-IMG_6885.jpeg)


A man on a city bike stopped and excitedly asked if we had any Allen keys; he wanted to adjust his seat post. He was absolutely delighted when we found one which fit, and we had done our good deed for the day!


![](/images/1690652863622-IMG_6886.jpeg)


We continued down the canal path, where inevitably Steve got another puncture: this time by a canoe hire store. Steve now deciding to skip the step of seeing if the sealant alone would fix the hole, went to work putting a tubeless tyre slug in. The canoe hire staff drank coffee and watched with mild interest. 

Gingerly inflating the tyre to the Goldilocks zone, not so high the sealant and slug erupts out, but not so soft that the rims hit the road. We continued, cycling through a beautiful forested area, which was very popular for a Saturday mid morning. Once we left the forest at the other end, we had a great little descent, where Steve…..got another puncture. Now an expert at fixing punctures, he leapt into action, putting another tubeless repair slug in. We are still perplexed by questions: why was it always his bike? His tyres, front and back?  

Here ends the first half of the day. 


We left the suburbs of greater Paris and we knew instantly we were out, as the roads became super smooth. Bumps which had become normalised now were barely seen at all. The route took us on quiet smooth roads which we were able to whizz through, which no waiting around for traffic lights. 


Progress was faster, and we stopped in a ‘co-op’ style shop in a village at 12.15 for lunch. We lucked out, as whilst we were munching on our sandwiches outside, the shutters came down on the shop door. We then noticed the sign; open until 12.30, then closed until 3.30pm. Somehow we had got there just at the right time. 


![](/images/1690653021970-IMG_6887.jpeg)


We continued, and the wind was at our backs, sometimes pushing us along. The sun was shining and the rolling hills were fun to climb up, and then sail down. 


![](/images/1690653157920-IMG_6889.jpeg)


![](/images/1690653275451-IMG_6890.jpeg)


We even ended up on a disused railway cycle path, the tarmac flat and easy to ride on, taking us through beautiful woodlands. 


![](/images/1690653370124-IMG_6891.jpeg)

At one point we went past a monument on the side of the road; Steve translated it to be dedicated to all the dogs which have died in the wars…possibly. Still it was an interesting thing to travel past. 


![](/images/1690653545594-IMG_6893.jpeg)


A lot of France we have cycled through is used for intense agriculture, and this region was just the same, just the fields were massive, wheat, corn, pears and hemp were prevalent. Unfortunately this also meant no hedges: these would have been great for shade and for if we needed a quick loo break. 


The sky behind us started to look a bit grim 20km away from Soissons; where our Ibis accommodation was for the night. The clouds were grey, and thunder and lightning could be seen  and heard. Somehow we were still in sunshine, but it was only a matter of time. We pushed ourselves a little harder. 


![](/images/1690653748622-IMG_6895.jpeg)


On the outskirts of Soissons, as we were navigating to the hotel, the rain began, we pushed harder than before and (avoiding a steep curb) reached the hotel just as the heavens opened. We were super lucky to have made it with clothes only enduring a few drips! Steve’s bag system would not have been able to sustain another deluge. A massive room on the ground floor awaited us and the bikes fit in with ample space to spare!!


![](/images/1690653907476-IMG_6896.jpeg)


The rain viewed from the comfort of the hotel’s window. 
